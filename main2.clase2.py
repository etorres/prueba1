class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

empleadoPepe = Empleado("Pepe", 20000)
empleadaAna = Empleado("Ana",30000)
print("El nombre del empleado es{} y su salario es {}".format(empleadoPepe.nombre, empleadoPepe.nomina))

#2 instancias, una de cada nombre del empleadoPepe y del empleadaAna
#1 clase, empleado 
#python tutor para ver que se va haciendo
#constructor es un metodo
#objeto es 